# ProfilerAPI2 <a href="https://www.genedata.com/products-services/profiler"><img src="inst/icons/logo.png" align="right" height="140" /></a>
<!--
If you edit this markdown, make sure to also edit Profiler API2 documentation in confluence
-->

## Overview
The `ProfilerAPI2` package is designed to make it easy to access the
[Genedata Profiler](https://www.genedata.com/products-services/profiler) API from R,
This package contains functions to access studies, up- and download data lake objects,
run pre-configured workflows, and query analysis-ready datasets. To see
all functions that are currently available in the API, run `help(package = "ProfilerAPI2")`.
The package changelog is available in the [NEWS.md](NEWS.md) file.

## Installation

The ProfilerAPI2 package is currently not available via CRAN but only from within a
[Genedata Profiler](https://www.genedata.com/products-services/profiler) Analytics Environment.
Within a Profiler Analytics environment, you can install the released version of `ProfilerAPI2`
from the pre-configures R package manager:

```R
install.packages('ProfilerAPI2')
```

## Usage

To use the `ProfilerAPI2` package, a running Genedata Profiler server is required. In the
Genedata Profiler Analytics Environment, the following environment variables specifying the
Profiler server URL and REST service are pre-configured:

* `PROFILER_URL`
* `PROFILER_CLIENT_ID`
* `PROFILER_CLIENT_SECRET`
* `PROFILER_HELP_URL`

After creating an API client object and authenticating against the Genedata Profiler server, you
can use the API client object to interact with the API (e.g. list studies).

```r
library(ProfilerAPI2)

# Create an API object and authenticate
api <- profiler_api()
api$authenticate()

# List accessable studies
api$studies$list()
```

For more in-depth tutorials and examples, you can refer to the cheat sheet and the package vignette.

 - <a href="inst/doc/Cheatsheet.pdf">Cheat Sheet</a>
 - <a href="https://genedata-profiler.gitlab.io/r-packages/profiler-api">ProfilerAPI2 Vignette</a>

The ProfilerAPI2 package vignette can also be viewed from within R with the command `browseVignettes("ProfilerAPI2")`.


## Testing

In order to run all tests that belong to the `ProfilerAPI2` package, you have to install the package
including tests.

``` r
install.packages("ProfilerAPI2", INSTALL_opts = "--install-tests")
```

Prior to running the test, please ensure that there is a dedicated test study and test user, which you
provide via the following environment variables in your R session:
* `PROFILER_TEST_STUDY_ID`
* `PROFILER_TEST_USER`
* `PROFILER_TEST_PASSWORD`

Additionally, the test user must be member of the test study with a study role with the
following permissions:

* Data
  * Create Data
  * Upload Data
  * Edit ungoverned metadata
  * Download data from REST API
  * Access files
  * Access analytics tables
  * Access analytics views
  * Access data without access tags
  * See analytics view content
* Software Development
  * Edit Integration workflows
  * Edit Genome workflows
* Analytics
  * Create analytics tables
  * Publish analytics views
* Access
  * Access REST API and Analytics DB
* Advanced Management
  * Manage global analytics tables


The tests belonging to the `ProfilerAPI2` package can now be run via `testthat`:
``` r
testthat::test_package("ProfilerAPI2")
```
