\documentclass{article}
\usepackage{pdfpages}
%\VignetteIndexEntry{Cheatsheet}

\begin{document}
\SweaveOpts{concordance=TRUE}
\includepdf[pages=-, fitpaper=true, landscape=true]{ProfilerAPI2_Cheatsheet.pdf}
\end{document}