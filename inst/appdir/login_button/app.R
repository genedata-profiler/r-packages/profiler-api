library(shiny)
library(ProfilerAPI2)

# UI
ui <- shiny::fluidPage(
  ProfilerAPI2::profiler_auth_mod_ui("gprofiler_login"),
  shiny::titlePanel(title = "Template Shiny App for Genedata Profiler Analytics Layer",
                    windowTitle = "Shiny Template"),
  shiny::sidebarLayout(
    shiny::sidebarPanel(shiny::uiOutput("select_view")),
    shiny::mainPanel(shiny::tableOutput("table"),
                     shiny::textOutput("authenticated"))
  )
)

# SERVER
server <- function(input, output, session) {

  # Get an authenticated api object
  api <- ProfilerAPI2::profiler_auth_mod_server(id = "gprofiler_login", ui_element = "button")

  # List views for authenticated user
  views <- shiny::reactive({
    shiny::req(api()$is_authenticated())
    api()$analytics_views$list()$id
  })

  # Fetch view and render the first 10 rows
  resultsTable <- shiny::reactive({
    shiny::req(api()$is_authenticated(), input$view, input$view != "<Select View>")
    api()$analytics_views$fetch(input$view)
  })

  # Render UI Output for view selection
  output$select_view <- shiny::renderUI({
    shiny::req(api()$is_authenticated(), views())
    shiny::selectizeInput(
      inputId = "view",
      label = "Analytics View",
      choices = c("Select a view..." = "", views()),
      selected = NULL
    )
  })

  # Render Table Output
  output$table <- shiny::renderTable({
    shiny::req(api()$is_authenticated(), resultsTable())
    rowId <- min(10, nrow(resultsTable()))
    resultsTable()[1:rowId,]
  })

  output$authenticated <- shiny::renderText({
    paste("The user has authenticated: ", api()$is_authenticated())
  })
}

# Run the app
shiny::shinyApp(ui = ui, server = server)

