#############################################################################
##
## (c) Genedata AG, Basel; profiler-helpdesk@genedata.com
##
#############################################################################


convert_type_profiler_to_r <- function(profiler_type) {
  switch(profiler_type,
    "SMALLINT" = "integer", # nocov
    "INTEGER" = "integer",
    "BIGINT" = "double",
    "REAL" = "double",
    "DOUBLE PRECISION" = "double",
    "BOOLEAN" = "logical",
    "VARCHAR" = "character",
    "DATE" = "date",
    "TIMESTAMP" = "datetime",
    stop("Unsupported type", call. = FALSE) # nocov
  )
}


convert_type_profiler_to_readr <-  function(profiler_type) {
  switch(profiler_type,
    "SMALLINT" = "i", # nocov
    "INTEGER" = "i",
    "BIGINT" = "d",
    "REAL" = "d",
    "DOUBLE PRECISION" = "d",
    "BOOLEAN" = "l",
    "VARCHAR" = "c",
    "DATE" = "D",
    "TIMESTAMP" = "T",
    stop("Unsupported type", call. = FALSE) # nocov
  )
}


convert_type_r_to_profiler <- function(obj) {
  switch(typeof(obj),
    factor = "VARCHAR(65535)",
    datetime = "TIMESTAMP",
    date = "DATE",
    integer = "INTEGER",
    double = "FLOAT8",
    character = "VARCHAR(65535)",
    logical = "BOOLEAN",
    list = "VARCHAR(65535)",
    time = ,
    binary = ,
    stop("Unsupported type", call. = FALSE) # nocov
  )
}

convert_rfc3339_to_posixct <- function(x) {
  as.POSIXct(x, format = "%Y-%m-%dT%H:%M:%SZ")
}

convert_datetimes_list <- function(x, names) {
  mapply(function(name, value) {
    if (name %in% names) {
      convert_rfc3339_to_posixct(value)
    } else {
      value
    }
  }, names(x), x, SIMPLIFY = FALSE)
}

convert_datetimes_tibble <- function(x, names) {
  datetime_columns <- intersect(colnames(x), names)
  if (length(datetime_columns) > 0) {
    dplyr::mutate(x, across(all_of(datetime_columns), convert_rfc3339_to_posixct))
  } else {
    x
  }
}