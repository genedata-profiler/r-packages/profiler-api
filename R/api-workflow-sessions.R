#############################################################################
##
## (c) Genedata AG, Basel; profiler-helpdesk@genedata.com
##
#############################################################################


#' WorkflowSessions
#'
#' @description The WorkflowSessions-object provides the functionality to
#' handle workflow sessions started via the REST API within Genedata Profiler.
#' The WorkflowSessions is a sub-API of the
#' \code{\link[=ProfilerAPI]{ProfilerAPI}} and gets authorization
#' credentials from it's parent ProfilerAPI-object. Please do not actively
#' create new sub-API objects, but call them via
#' \code{ProfilerAPI$workflow_sessions}.
#'
#' @section WorkflowSessions Method Overview:
#' The following functions can be used via \code{\link[=WorkflowSessions]{ProfilerAPI$workflow_sessions$...}}:
#' \itemize{
#'  \item \code{run(workflow_configuration)}: Start the execution of a workflow
#'      on the Genedata Profiler server.
#'  \item \code{list(module = NULL)}: List all workflow sessions started via the
#'      REST API for the authenticated user, filtered by module. Note: The module
#'      parameter only takes effect from server versions of Genedata Profiler >= 15.0.7.
#'      For earlier server versions, the workflow session listing will always only
#'      return Integration workflow sessions.
#'  \item \code{details(session_id)}: Provides information about a workflow
#'      session, such as user, study, start date and time, and workflow
#'      execution status.
#'  \item \code{delete(session_id)}: Abort execution of a running workflow
#'      session and/or delete a finished workflow session.
#'  \item \code{wait_for(session_id, verbose = TRUE, polling_interval = 3)}:
#'      Repeatedly checks the workflow execution status. Only when the workflow
#'      is not running anymore, the function finishes and returns the final
#'      workflow execution status.
#' }
#'
#' @param session_id The workflow session id.
#'
#' See also the \code{vignette("Automation", package = "ProfilerAPI2")}
#' vignette on how to use workflows and workflow sessions.
#' @docType class
#' @seealso \link{ProfilerAPI}
#' @format An R6 class object.
#' @importFrom R6 R6Class
WorkflowSessions <- R6::R6Class("WorkflowSessions",
  cloneable = FALSE,

  private = list(
    api = NULL,
    column_names = c("id", "name", "module", "user", "study", "progress", "started", "state", "message", "error"),
    sessions_url = function(session_id = NULL) {
      url <- paste0(private$api$api_url, "/workflow-sessions")
      if (!is.null(session_id)) {
        url <- paste0(url, "/", session_id)
      }
      url
    },
    get_session_state = function(session_id) {
      self$details(session_id)$state
    }
  ),

  public = list(
    #' @description
    #' Create a new WorkflowSessions object.
    #' @param api An instance of a \code{\link[=ProfilerAPI]{ProfilerAPI}} object.
    initialize = function(api) {
      stopifnot(inherits(api, "ProfilerAPI"))
      private$api <- api
    },

    #' @description Prints the WorkflowSessions object.
    #' @param ... All extra arguments from a print(obj, ...) call are passed on
    #'   to the obj$print(...) method.
    print = function(...) {
      cat("<WorkflowSessions>\n", sep = "")
      invisible(self)
    },

    #' @description On the Genedata Profiler server, a local copy of the
    #' referenced workflow will be generated and customized with the provided
    #' \code{workflow_configuration} before executing the workflow. This only
    #' works for workflows in Data-Lake studies.
    #' Note: The `run` function can also be called from the
    #' \code{\link[=Workflows]{Workflows}}.
    #' @param workflow_config A fully configured
    #'   \code{\link[=WorkflowConfiguration]{WorkflowConfiguration}} object.
    #' @return Returns the workflow session id of the active workflow.
    run = function(workflow_config) {
      check_required_parameter(workflow_config, "workflow_config")
      stopifnot(inherits(workflow_config, "WorkflowConfiguration"))

      response <- httr::POST(
        private$sessions_url(),
        config = private$api$token,
        body = workflow_config$to_json(),
        encode = 'raw',
        httr::content_type('application/json')
      )

      check_response(response)
      response_content <- httr::content(response, as = "text", type = "application/json", encoding = "UTF-8")
      jsonlite::fromJSON(response_content)$id
    },

    #' @description List all workflow sessions started via the REST
    #' API for the authenticated user.
    #' @param module If provided, only workflows for this module are listed.
    #' @return Returns workflow sessions started via the REST API for the
    #'   authenticated user.
    list = function(module = NULL) {
      query_parameters <- list()
      if (!is.null(module)) {
        server_version <- private$api$server_version
        if (compareVersion(server_version, "15.0.7") == -1) { # nocov start
          warning("Your Profiler version (", server_version,
                  ") does not support listing of workflow sessions for",
                  " other modules than Integration. The 'module' parameter",
                  " does not have an effect before Profiler 15.0.7.", call. = FALSE)
        } else { # nocov end
          check_module(module)
        }
        query_parameters[["module"]] <- tolower(module)
      }

      response <- httr::GET(private$sessions_url(), config = private$api$token, query = query_parameters)
      check_response(response)
      response_content <- httr::content(response, as = "text", type = "application/json", encoding = "UTF-8")

      columns <- tibble::tibble(
        id = character(0),
        module = character(0),
        user = character(0),
        study = character(0),
        progress = character(0),
        started = character(0),
        name = character(0),
        status.description = character(0),
        status.error = character(0),
        status.state = character(0)
      )

      dplyr::bind_rows(columns, tibble::as_tibble(jsonlite::fromJSON(response_content, flatten = TRUE)$sessions)) %>%
        rename(message = status.description, error = status.error, state = status.state) %>%
        convert_datetimes_tibble(names = "started") %>%
        dplyr::relocate(dplyr::any_of(private$column_names)) %>%
        dplyr::mutate_at("error", list(~dplyr::na_if(., "")))
    },

    #' @description Provides information about a workflow session, such as user,
    #' study, start date and time, and workflow execution status.
    #' @param session_id The session id of the workflow session.
    details = function(session_id) {
      check_required_parameter(session_id, "session_id")
      response <- httr::GET(private$sessions_url(session_id), config = private$api$token)
      check_response(response)
      response_content <- httr::content(response, as = "text", type = "application/json", encoding = "UTF-8")

      details <- convert_datetimes_list(jsonlite::fromJSON(response_content), names = "started")
      details <- c(details[-which(names(details) == "status")], unlist(details$status))
      names(details)[which(names(details) == "description")] <- "message"
      details$error <- ifelse(is_empty(details$error), NA, details$error)
      details[private$column_names]
    },

    #' @description Abort execution of a running workflow
    #'      session and/or delete a finished workflow session.
    #' @param session_id The session id of the workflow session.
    delete = function(session_id) {
      check_required_parameter(session_id, "session_id")
      response <- httr::DELETE(private$sessions_url(session_id), config = private$api$token)
      check_response(response)
      httr::content(response, as = "text", type = "application/json", encoding = "UTF-8")
    },

    #' @description This function repeatedly checks the workflow execution
    #' status. Only when the workflow is not running anymore, the function
    #' finishes and prints the final workflow execution status.
    #' @param ... The session IDs of workflow session you want to wait
    #'   for.
    #' @param verbose Should the function print update messages while waiting
    #'   for the workflow to finish?
    #' @param polling_interval The number of seconds in between status checks.
    #' @return Returns the final workflow status, e.g. "Complete".
    wait_for = function(..., verbose = TRUE, polling_interval = 3) {
      session_ids <- unlist(list(...))
      stopifnot(length(session_ids) > 0)

      states <- session_ids %>% purrr::map_chr(private$get_session_state)

      while (any(is.element(states, "Running"))) {
        if (verbose) {
          for (i in seq_along(session_ids)) {
            message("Workflow session '", session_ids[i], "' is '", states[i], "' ...")
          }
        }
        Sys.sleep(polling_interval)
        states <- purrr::map2_chr(session_ids, states, function(id, s) {
          if (s != "Running") {
            return(s)
          }
          private$get_session_state(id)
        })
      }

      if (verbose) {
        for (i in seq_along(session_ids)) {
          if (states[i] == "Complete") {
            message("Workflow session '", session_ids[i], "' completed successfully.")
          } else {
            message("Workflow session '", session_ids[i], "' finished with status '", states[i], "'")
          }
        }
      }

      set_names(states, session_ids)
      states
    }
  )
)


workflow_sessions <- function(api) {
  WorkflowSessions$new(api)
}
