#############################################################################
##
## (c) Genedata AG, Basel; profiler-helpdesk@genedata.com
##
#############################################################################


#' AnalyticsViews
#'
#' @description The AnalyticsViews-object provides the functionality to
#' retrieve information on analytics views registered within Genedata Profiler.
#' The AnalyticsViews is a sub-API of the
#' \code{\link[=ProfilerAPI]{ProfilerAPI}} and gets authorization
#' credentials from it's parent ProfilerAPI-object. Please do not actively
#' create new sub-API objects, but call them via
#' \code{ProfilerAPI$analytics_views}.
#'
#' @section AnalyticsViews Method Overview:
#' The following functions can be used via \code{\link[=AnalyticsViews]{ProfilerAPI$analytics_views$...}}:
#' \itemize{
#'  \item \code{list(study_id = NULL, metadata = list(), pattern = NULL, latest_only = FALSE, executable_only = TRUE,
#'      all_attributes = FALSE, fetch_size = 200)}:
#'      Returns a tibble of accessible views, optionally restricted to a study, the latest version of views, only executable,
#'      and filtered by metadata or a name pattern.
#'  \item \code{details(view_id)}: Returns a named list, listing view details
#'      such as its name, column count, and description, schema, and metadata.
#'  \item \code{fetch(view_id, view_parameters = list())}: Retrieves a view
#'      from the Genedata Profiler server.
#'  \item \code{view_id(view_name, study_id = NULL, ignore_case = TRUE)}:
#'      Returns the matching latest view id for a specific view name.
#'  \item \code{create_view_from_table(object_id, folder_id, name = NULL, description = NULL,
#'              metadata = list(), overwrite = NA, access_tags = NULL)}:
#'      Publish a new analytics view for an existing analytics table.
#'  \item \code{create_view_from_sql(sql, folder_id, name, description = NULL, metadata = list(), overwrite = NA,
#'      access_tags = NULL)}: Publish an SQL-query as a new analytics view.
#' }
#' @docType class
#' @seealso \link{ProfilerAPI}
#' @format An R6 class object.
#' @importFrom R6 R6Class
#' @importFrom vroom vroom
AnalyticsViews <- R6::R6Class("AnalyticsViews",
  cloneable = FALSE,

  private = list(
    api = NULL,
    views_url = function() {
        paste0(private$api$api_url, "/analytics/views")
    },
    objects_url = function() {
        paste0(private$api$api_url, "/objects")
    }
  ),

  public = list(
    #' @description Create a new AnalyticsViews object.
    #' @param api An instance of a
    #'   \code{\link[=ProfilerAPI]{ProfilerAPI}} object.
    initialize = function(api) {
        stopifnot(inherits(api, "ProfilerAPI"))
        private$api <- api
    },

    #' @description Prints the AnalyticsViews object.
    #' @param ... All extra arguments from a print(obj, ...) call are passed on
    #'   to the obj$print(...) method.
    print = function(...) {
        cat("<AnalyticsViews>\n", sep = "")
        invisible(self)
    },

    #' @description List all accessible views for that user. Optionally, the
    #' views can be restricted to a specific study or filtered by metadata.
    #' @param study_id If provided, only accessible views for the respective
    #'   study are listed.
    #' @param metadata Allows to optionally filter by matching metadata
    #'   key-value pairs, which should be provided as a named list.
    #' @param pattern Optional pattern to filter the resulting view list. See
    #'  the pattern option in \code{\link[stringr:str_detect]{stringr::str_detect}}
    #'  for more information.
    #' @param latest_only Should only latest view versions be listed? FALSE by default.
    #' @param executable_only Should only executable views be listed? TRUE by default.
    #' @param all_attributes Should all metadata attributes be included in the result?
    #' @param fetch_size Result objects are fetched in chunks of provided size.
    #' The maximum chunk size is 1000 (default: 200)
    #' @return Returns a tibble of accessible views.
    list = function(study_id = NULL, metadata = list(), pattern = NULL, latest_only = FALSE, executable_only = TRUE,
                    all_attributes = FALSE, fetch_size = 200) {
        check_metadata(metadata)
        stopifnot(is.logical(latest_only))
        checkmate::assertFlag(executable_only)

        metadata <- update_metadata_for_filter_query(metadata)
        query_parameters <- metadata  # this is already a (maybe empty) list
        if (!is.null(study_id)) {
            query_parameters[["study"]] <- study_id
        }
        query_parameters[["onlyLatest"]] <- tolower(as.character(latest_only))
        query_parameters[["onlyExecutable"]] <- tolower(as.character(executable_only))

        rest_response <- httr::GET(private$views_url(), config = private$api$token, query = query_parameters)
        check_response(rest_response)

        response_content <- httr::content(rest_response, as = "text", type = "application/json", encoding = "UTF-8")
        views_df <- tibble::as_tibble(jsonlite::fromJSON(response_content)$views)

        columns <- tibble::tibble(
          id = character(0),
          name = character(0),
          studyId = character(0),
          studyName = character(0),
          baseId = character(0),
          version = integer(0),
          latestViewAlias = character(0),
          latest = logical(0),
          objectId = character(0),
          parent = character(0),
          description = character(0),
          created = character(0),
          lastUpdated = character(0),
          creator = character(0),
          columnCount = integer(0),
          rowCount = integer(0),
          referencedViews = list(),
          parameters = list(),
          studyGroup = character(0),
          studyDescription = character(0),
          tags = list(), # lists accessTags in old format for backwards-compatibility
          accessTags = list()
        )

        res <- dplyr::bind_rows(columns, tibble::as_tibble(views_df)) %>%
          filter_name_or_id_by_pattern(pattern) %>%
          convert_datetimes_tibble(names = c("lastUpdated", "created")) %>%
          # ensure that accessTags col is a column of type list<data.frame>
          ensure_access_tags_dataframe(.)

        if (nrow(res) == 0){
            return(res)
        }

        if (all_attributes) {
            metadata_df <- list_with_all_attributes(res$objectId, private$objects_url(), private$api$token,
                                                    fetch_size, only_metadata=T)
            res <- dplyr::left_join(res, metadata_df, by = dplyr::join_by("objectId"=="id"), keep=FALSE)
        }

        res
    },


    #' @description Get the view details for a specific view.
    #' @param view_id  The view id of the analytics view.
    #' @return Returns a named list, listing view details such as its name,
    #'   column count, and description, schema, accessTags and metadata.
    details = function(view_id) {
        rest_response <- httr::GET(
          paste0(private$views_url(), "/", view_id),
          config = private$api$token,
          query = list("includePaths" = "true")
        )
        check_response(rest_response)

        response_content <- httr::content(rest_response, as = "text", type = "application/json", encoding = "UTF-8")
        convert_datetimes_list(jsonlite::fromJSON(response_content), names = "created")
    },

    #' @description Retrieves a view from the Genedata Profiler server.
    #' @param view_id  The view id of the analytics view.
    #' @param view_parameters If the view was generated with parameters, values
    #'   for each required parameter must be provided in a named list, e.g.
    #'   \code{view_parameters=list("gene"="BRAF")}.
    #' @return Returns a tibble with the data in the view.
    fetch = function(view_id, view_parameters = list()) {
        details <- self$details(view_id)
        real_view_params <- details$parameters
        if (length(view_parameters) > 0) {
            names(view_parameters) <- tolower(names(view_parameters))
        }

        if (length(real_view_params) > 0) {
            real_view_params$name <- tolower(real_view_params$name)
            common_parameters <- intersect(real_view_params$name, names(view_parameters))
            if (length(common_parameters) < nrow(real_view_params)) {
                stop("Please provide values for all view parameters: ", real_view_params$name)
            }
            if (length(common_parameters) < length(view_parameters)) {
                message("The following parameters you provided were ignored: ", setdiff(names(view_parameters), common_parameters))
            }
            view_parameters <- view_parameters[common_parameters]
        }

        url <- paste0(private$views_url(), "/", view_id, "/fetch")
        tryCatch({
            rest_response <- httr::GET(url, config = private$api$token, query = view_parameters)
        }, error = function(e) {
            if (grepl("transfer closed with outstanding read data remaining", conditionMessage(e), fixed = TRUE)) {
              e <- "The returned result was truncated. The underlying query was likely cancelled, possibly due to a timeout."
            }
            stop("Error fetching view: ", e)
        })
        check_response(rest_response)

        view_schema <- details$schema
        col_types <- paste(sapply(view_schema$type, convert_type_profiler_to_readr, USE.NAMES = FALSE), collapse = "")

        response_content <- httr::content(rest_response, "raw")
        vroom::vroom(
          I(response_content),
          delim = "\t",
          col_names = TRUE,
          col_types = col_types,
          na = "",
          quote = "\"",  # REST-API view endpoints quote values with double quotes if necessary
          guess_max = -1,
          num_threads = 1,
          skip_empty_rows = FALSE,
          trim_ws = FALSE
        )
    },

    #' @description Get the view id for a specific view name.
    #' @param view_name The name of the view.
    #' @param study_id Optionally, the study id in which the view is visible.
    #' @param ignore_case Should the lookup be done case-insensitive?
    #' @return Return the view id for the corresponding view name. If there
    #' are multiple matching view ids for a given \code{view_name} the if for the
    #' view created latest is returned.
    view_id = function(view_name, study_id = NULL, ignore_case = TRUE) {
        check_required_parameter(view_name, "view_name")
        if (ignore_case) {
            view_name <- tolower(view_name)
        }

        views <- self$list(study_id) %>%
        { if (ignore_case) mutate(., name = tolower(name)) else . } %>%
          filter(name == view_name) %>%
          top_n(., n = 1, created) %>%
          pull(id)

        if (length(views) == 0) {
            warning("No view found with name '", view_name, "'.", call. = FALSE)
            return(NULL)
        }
        views
    },

    #' @description Publish a new analytics view for an existing analytics table. Note that
    #' the new view will give access to all rows and columns of the provided table.
    #' @param object_id Object id of an analytics table.
    #' @param folder_id The parent folder id, or a study id, in which
    #'   the analytics table should be created. By default, the view will be
    #'   created in the same folder location as the provided analytics table.
    #'   If a valid study id is provided, the view will be created the study's root
    #'   folder.
    #' @param name The name of the created analytics view. Note that the view id
    #' will be derived from this name. By default, the view will be named identical to
    #' the corresponding analytics table.
    #' @param description Optional description of the analytics view.
    #' @param metadata The analytics table can optionally be tagged with metadata.
    #' Note that any \code{metadata} must be provided as a named list mapping attributes
    #' to character vector values.
    #' @param overwrite Logical, indicating whether to check for a pre-existing view with the same name
    #' in the parent folder.
    #' \itemize{
    #'    \item{`NA` (default):} {Just create the view. In case of pre-existing view(s), the view is
    #'      automatically renamed by appending [1] to the view name.}
    #'   \item{`TRUE`:} {Check for a pre-existing view with the specified name in the target folder.
    #'     If there is one, move the pre-existing view to the Recycle Bin, then carry on. Note
    #'     that the new view does not inherit any metadata attributes from the old one. It will
    #'     have a new object id.}
    #'   \item{`FALSE`:} {Error if there is any pre-existing view with the specified name in the target
    #'      folder.}
    #' }
    #' @param access_tags Optional. Either a vector of access tag ids or a data.frame of access tags as returned by
    #'   \code{\link[=AnalyticsViews]{details()}}.
    #' @return Returns the view id of the newly created analytics view.
    create_view_from_table = function(object_id, folder_id, name = NULL, description = NULL, metadata = list(),
                                      overwrite = NA, access_tags = NULL) {
        check_required_parameter(object_id,"object_id", "Please provide an object id of an analytics table.")
        stopifnot(startsWith(object_id, "table"))
        check_access_tags(access_tags)
        check(checkmate::check_flag(overwrite, na.ok = TRUE), "overwrite")

        if (is.null(name)) {
          name <- private$api$data_lake$details(object_id)$name
        }
        self$create_view_from_sql(sql = glue::glue('SELECT * FROM "{table_name}"', table_name = object_id),
                                  folder_id = folder_id, name = name, description = description, metadata = metadata,
                                  overwrite = overwrite, access_tags = access_tags)
    },

    #' @description Publish a new analytics view for an existing analytics table. Note that
    #' the new view will give access to all rows and columns of the provided table.
    #' @param sql Character string representing containing the SQL query to be saved as new view.
    #' @param folder_id The parent folder id, or a study id, in which
    #'   the analytics table should be created.
    #' @param name The name of the created analytics view. Note that the view id
    #' will be derived from this name.
    #' @param description Optional description of the analytics view.
    #' @param metadata The analytics table can optionally be tagged with metadata.
    #' Note that any \code{metadata} must be provided as a named list mapping attributes
    #' to character vector values.
    #' @param overwrite Logical, indicating whether to check for a pre-existing view with the same name
    #' in the parent folder.
    #' \itemize{
    #'    \item{`NA` (default):} {Just create the view. In case of pre-existing view(s), the view is
    #'      automatically renamed by appending [1] to the view name.}
    #'   \item{`TRUE`:} {Check for a pre-existing view with the specified name in the target folder.
    #'     If there is one, move the pre-existing view to the Recycle Bin, then carry on. Note
    #'     that the new view does not inherit any metadata attributes from the old one. It will
    #'     have a new object id.}
    #'   \item{`FALSE`:} {Error if there is any pre-existing view with the specified name in the target
    #'      folder.}
    #' }
    #' @param access_tags Optional. Either a vector of access tag ids or a data.frame of access tags as returned by
    #'   \code{\link[=AnalyticsViews]{details()}}.
    #' @examples
    #' \dontrun{
    #' api$analytics_views$create_view_from_sql(
    #'  'SELECT * FROM "table-12345"',
    #'  folder_id = 'folder-12345',
    #'  name = 'View From SQL'
    #'  )
    #' }
    #' @return Returns the view id of the newly created analytics view.
    create_view_from_sql = function(sql, folder_id, name, description = NULL, metadata = list(), overwrite=NA, access_tags = NULL) {
        check_required_parameter(sql,"sql", "Please provide a sql query to be saved as view.")
        check_required_parameter(folder_id,"folder_id", "If a valid study id is provided instead of a folder id, the view will be created in the study's root folder.")
        check_required_parameter(name,"name", "Please provide a view name.")
        check_metadata(metadata)
        check_access_tags(access_tags)
        check(checkmate::check_flag(overwrite, na.ok = TRUE), "overwrite")

        apply_overwrite_policy(name, folder_id, overwrite, private$api)

        body <- list(
          query = sql,
          parent   = to_folder_id(folder_id, private$api),
          name     = name,
          rename   = TRUE,
          metadata = lapply(lapply(metadata, unlist), unname),
          accessTagIds = get_access_tag_ids(access_tags),
          viewDescription = description
        )
        response <- httr::POST(
          private$views_url(),
          body = body,
          encode = "json",
          config = private$api$token
        )
        check_response(response)

        content <- httr::content(response, encoding = "UTF-8")
        poll_url <- glue::glue("{base_url}/analytics/views/status/{id}", base_url = private$api$api_url, id = content$objectId)
        poll_for_commit_resolution(content, poll_url, private$api$token)

        # long running commits may not return a view id
        if (is.null(content$id)) {
            return(self$details(content$objectId)$id)
        }

        content$id
    }
  )
)


analytics_views <- function(api) {
    AnalyticsViews$new(api)
}
