#############################################################################
##
## (c) Genedata AG, Basel; profiler-helpdesk@genedata.com
##
#############################################################################


#' Admin
#'
#' @description The Admin-object provides the functionality to manage study roles and
#' global analytics tables. The Admin section is a sub-API of the
#' \code{\link[=ProfilerAPI]{ProfilerAPI}} and gets authorization
#' credentials from it's parent ProfilerAPI-object. Please do not actively
#' create new sub-API objects, but call them via
#' \code{ProfilerAPI$admin}.
#'
#' @section Admin Method Overview:
#' The Admin object provides access to the sub-APIs for Study Role Management and
#' Global Analytics Tables
#'
#' @inheritSection StudyRoles StudyRoles Method Overview
#' @inheritSection GlobalAnalyticsTables GlobalAnalyticsTables Method Overview
#'
#' @docType class
#' @seealso \link{ProfilerAPI}
#' @format An R6 class object.
#' @export
#' @importFrom R6 R6Class
Admin <- R6::R6Class(
  "Admin",
  cloneable = FALSE,
  private = list(
    api = NULL,
    # scope-specific APIs
    .study_roles = NULL,
    .global_tables = NULL
  ),
  public = list(
    #' @description
    #' Create a new Admin object.
    #' @param api An instance of a
    #'   \code{\link[=ProfilerAPI]{ProfilerAPI}} object.
    initialize = function(api) {
      stopifnot(inherits(api, "ProfilerAPI"))
      private$api <- api
      private$.study_roles <- study_roles(api)
      private$.global_tables <- global_analytics_tables(api)
    },

    #' @description Prints the Admin object.
    #' @param ... All extra arguments from a print(obj, ...) call are passed on
    #'   to the obj$print(...) method.
    print = function(...) {
      cat("<Admin>\n", sep = "")
      invisible(self)
    }
  ),
  # get/setter functions implemented as 'active fields'
  active = list(
    #' @field global_analytics_tables An instance of a
    #'   \code{\link[=GlobalAnalyticsTables]{GlobalAnalyticsTables}} object.
    global_analytics_tables = function(value) {
      if (missing(value)) {
        private$.global_tables
      } else {
        stop("`$global_analytics_tables` is read-only", call. = FALSE)
      }
    },
    # Module-specific APIs
    #' @field study_roles An instance of a
    #'   \code{\link[=StudyRoles]{StudyRoles}} object that allows to manage
    #'   study roles.
    study_roles = function(value) {
      if (missing(value)) {
        private$.study_roles
      } else {
        stop("`$study_roles` is read-only", call. = FALSE)
      }
    }
  )
)


admin <- function(api) {
  Admin$new(api)
}
