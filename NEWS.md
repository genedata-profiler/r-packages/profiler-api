# ProfilerAPI2 3.8.0 (2024-12-04)
* Render device authentication URL as clickable link.
* Explicitly cast the max_bytes parameter to integer in the api$data_lake$download_file_raw() method (if it is not Inf).
* Added support for updating access tags for Data Lake objects via `DataLake$update_access_tags()`.
* Vignette: Fix mentioned R API version regarding the introduction of the `accessTags` column.

# ProfilerAPI2 3.7.1 (2024-11-07)
* Added support for new `submissionTime` column in the return value of `DataLake$list_archival_operations`.
* The `startTime` column in the return value of `DataLake$list_archival_operations` no longer refers to time of submission, but instead either refers to the start time of currently ongoing retry or to the start time of the next scheduled retry. 

# ProfilerAPI2 3.7.0 (2024-10-02)
* Added functions `DataLake$list_archival_operations` and `DataLake$set_storage_class` for objects archival. Note that the Data Tier license key is required for this feature.
* Breaking change: the `api$authenticate` method now uses device flow instead of password flow for authentication by default. 
Authentication via username and password will still be possible but is deprecated, and will be removed in a future version.
* Allow to specify the trino catalog name via environment variable PROFILER_ANALYTICS_DB_TRINO_CATALOG. 
* Allow to specify a database configuration object and not only string for db_config parameter in profiler_api initialization.
* Allow to specify access tags in the table and view creation methods as well as in data uploading methods. 
* Report accessTags column in the table and view listing results as well as object listing when using `all_attributes` parameter. 
* Breaking change: change format of how access tags are returned and assigned in the study roles section.
* Added functions to fetch study-specific statistics on files, views and tables via `Studies$file_statistics()` , `Studies$view_statistics()` and `Studies$table_statistics()`.
* Added support for additional workflow configuration fields `BooleanField`, `ControlledGroupField`, `StringField`, `ScrollableTextField`, `ColumnChoosingField`, `IntegerField`, `FloatField`, `ColumnListSelectionField`, and `MultiChoiceIdField`.
* Added functions `set_field_values` and `as_tibble` to the `WorkflowConfiguration` class.
* Removed CM numbers present in test names.

# ProfilerAPI2 3.6.2 (2024-06-19) 
* Added backward compatibility of recursive data lake listing for Profiler versions prior to 16.4.

# ProfilerAPI2 3.6.1 (2024-06-14)
* Fixed an issue that would not allow to set multiple input files through the workflow config set_field_value() method.

# ProfilerAPI2 3.6.0 (2024-04-26)
* Added `append` argument to `AnalyticsTables$create_table_from_data_frame()` and `AnalyticsTables$create_table_from_file()` functions to allow for appending data to Analytics Tables.
* Added `AnalyticsTables$list()` function to list accessible Analytics Tables. 
* Added `AnalyticsTables$details()` function to fetch details about an Analytics Table.
* Added `AnalyticsTables$seal()` function to seal an appendable Analytics Table and make it immutable. 
* Added `AnalyticsTables$vacuum()` function to vacuum an appendable Analytics Table.
* Added `AnalyticsTables$statistics()` function to fetch sort statistics from internal Analytics Tables.
* Added new parameter `all_attributes` to `AnalyticsViews$list()` function to include also metadata columns in the data frame result.

# ProfilerAPI2 3.5.0 (2024-02-15)
* Breaking change: In `api$data_lake$download_file_raw` the `max_size` parameter has been superseded by a new `max_bytes` parameter which allows to specify the maximum number of bytes to read.
* Fixed a problem with recursive data lake listing that occurred when the study being listed has shared folders.
* Allow for moving data lake objects via new `api$data_lake$move()` function.
* Add ability to specify metadata validation policies when creating/updating a study.
* Added functionality to trigger metadata validation runs, list objects with invalid metadata and retrieve details for  metadata rules in the new section `api$metadata_validation`.
* Added new parameter `delete_permanently` to `api$data_lake$delete()`. If `TRUE`, object deletion is permanent.

# ProfilerAPI2 3.4.1 (2023-12-08)
* Fixed an issue that could result in an error when recursively listing all contents of a study or shared folder.
* Improved error checking of provided object IDs before downloading a file or a folder.
* Allow for passing named vector as source_ids argument to data lake `upload_file()` method.

# ProfilerAPI2 3.4.0 (2023-11-07)
* Breaking change: Support for environment variable PROFILER_SECRET_API_KEY has been removed in favor of environment variable 
  PROFILER_ACCESS_TOKEN containing a personal access token that can be created in the Profiler web app (requires Profiler 16.3).
* Changed the behaviour of the `api$analytics_views$list()` function: per default, only executable views are listed. Set the new parameter `executable_only` to `FALSE` to list all views.
* Added overwrite mode to table and view creation functions. The new default behaviour is to rename the created tables/views in case objects with same names already exist in the data lake target folder. Previously, an error was thrown.
* Replace metadata rule groups listing by listing of metadata editing permissions. In addition, use metadata editing permissions in functions for study role listing, creation and update.
* Revised vignette to combine all previously existing vignettes into one. Updated vignette with respect to latest changes in the ProfilerAPI2 package.
* Improve documentation for parameter source_ids.

# ProfilerAPI2 3.3.1 (2023-08-30)
* Data Lake listing now includes a `path` column. The `path` column contains the relative path to the respective object.
* Add ability to list data governance assets relevant for creating study roles.
* Fixed an error that could occur if the profiler_api() factory method is used in presence of a default profile without saved credentials.
* Minimum required package version of stringr is now 1.5.0

# ProfilerAPI2 3.3.0 (2023-06-19)
* Added the ability to list, add, and remove users or user groups from studies, using `api$studies$members$list()`, `api$studies$members$add()`, `api$studies$members$remove()` respectively.
* Added functionality to link federated tables into the Profiler data lake.
* Added functionality to manage study roles in the new admin section via `api$admin$study_roles`. 
* The global analytics tables sub API is now accessible via the admin section with `api$admin$global_analytics_tables`. Usage of `api$global_analytics_tables` is deprecated and will be removed in the future.
* Added functionality to create, delete and update studies.
* Changed signature of data lake upload functions such that the function argument to local object to uploaded comes before the target folder argument.
* Added overwrite mode to data lake upload functions. The new default behaviour is to rename the uploaded files/folders in case objects with same names already exist in the data lake target folder. Previously, an error was thrown. 
* Changed the behaviour of the `api$data_lake$create_folder()` function: if a folder with the same name already exists, the name of the newly created folder will be made unique by adding a suffix.
* Improved documentation for configurable activities in workflow configuration.
* Data Lake listing now includes a `path` column. The `path` column contains the relative path between the listed object and the specified folder.

# ProfilerAPI2 3.2.1 (2023-04-05)
* Added include and exclude pattern parameters to the data lake download folder function.
* Fixed an issue that could lead to errors when creating an API object from a profile containing an invalid refresh token.
* Fixed an issue when creating an API object with a config object that differs from the config in the environment variables.
* Improved documentation of profile_api() constructor method and profile_db_config() helper method.
* Updated Best Practice Vignette.

# ProfilerAPI2 3.2.0 (2023-01-31)
* Allow for Data Federation by adding functionality for Trino connection (requires Profiler >= 16.1.0).
* Added functionality to migrate a workflow xml file from the Profiler server via `Workflows$migrate()` (requires Profiler >= 16.0.10).
* Fixed bug when fetching object id from the root folder of a study.
* Fixed potential vroom warnings about using I() for literal data that could occur with vroom versions >= 1.5.0.
* Ensure that when using the auth-shiny and auth-sso-shiny modules the API object is not constructed from stored profiles.
* Fixed a problem that would erroneously load the default profile if the API is constructed with a config object.
* Improved some tests.

# ProfilerAPI2 3.1.0 (2022-08-12)
* Allow for creating Analytics Tables directly from tabular files or data frames via `AnalyticsTables$create_table_from_file` and `AnalyticsTables$create_table_from_data_frame`.
* Allow for creating Analytics Views directly from SQL via `AnalyticsViews$create_view_from_sql`.
* The `DataLakedownload_file()` and `DataLakedownload_folder()` functions skips already existing files if overwrite set to FALSE.
* The `profiler_api()` factory method now tries to automatically load the profile named 'default' if the profile argument is not provided 
  (the default profile name can be overridden by setting the environment variable `PROFILER_CONFIG_PROFILE`)
* The package now requires dbplyr version 2.2.0 as backend for SQL queries. This change resolves warnings about old dbplyr interfaces being used.
* Excluded cookies from the bookmark in the auth shiny modules.
* Fixed error message when delete endpoint is not in scope.
* Fixed problem in the `DataLake$upload_multiple_files()` that was failing when the parameter `rel_paths` was not specified.
* Added functionality to delete a workflow xml file from the Profiler server via `Workflows$delete()` (requires Profiler >= 16.0.8).
* Fixed problems in `Studies$study_id()` that was failing when the study name contained non-ASCII characters.

# ProfilerAPI2 3.0.4 (2022-06-17)
* Support metadata modification (add, set, remove, clear) via `DataLake$metadata_*` methods (requires Profiler >= 16.0.6).

# ProfilerAPI2 3.0.3 (2022-05-12)
* Fixed a problem in the `AnalyticsViews$fetch()` method that could lead to cell values with erroneously trimmed leading or trailing whitespace characters.
* Support query parameters in redirect triggered by shiny auth modules (requires Profiler >= 16.0.4).
* Add functions for manual logout and re-login when using `profiler_sso_mod`.
* Improved the `DataLake$upload` functions to prevent potential timeout errors when uploading large files.
* Print warnings if objects can't be accessed or found when calling `DataLake$delete()`.

# ProfilerAPI2 3.0.2 (2022-03-10)
* Added a `DataLake$delete()` function that allows for deleting one or more objects in batch and puts them into the Recycle Bin.
* Adjusted tests/workflows used in tests so that they can be run in different studies (follow-up from View Versioning).
* Improved test runtime so that data will only be uploaded to the data lake if required or if the environment variable SKIP_DATA_SETUP = "true".
* Removed defunct and deprecated methods.
* Updated an error message in case a user does not have access to a study/folder id but provides it in data lake functions.

# ProfilerAPI2 3.0.1 (2022-01-21)
* Auto-connect to Profiler Analytics Database when accessing active binding `ProfilerAPI$conn`.
* Added a `latest_only` argument to `AnalyticsViews$list()` to list only latest views. The view listing now also reports additional columns like the latest, lastestViewAlias, baseId, version. 
* View fetching via `AnalyticsViews$fetch()` now fails properly when the REST API returns an error, and does not only print the error message.
* View fetching via `AnalyticsViews$fetch()` shows an improved error message in case of query cancellation due to timeouts.
* The optional pattern matching argument in listing methods is now also applied to the id column and not just the name column.
* Added new section to best practices on how to package Shiny apps.
* Use POSIXct data type for datetime values also for the created property in `DataLake$details()`.
* Updated tests for changed REST API functionality: source metadata are attached to uploaded files.
* Improve type check of provided values for character vector arguments for constructor and setter methods of the ObjectSelectionField, ObjectNameField, and ExternalFileSelectionField.
* Improved order of printing messages during file upload to fix rare race condition in tests.
* Adjusted tests to latest changes in REST API.

# ProfilerAPI2 3.0.0 (2021-11-05)
* Replaced ProfilerAnalyticsConnection with a RPostgres DBI connection to communicate with the Profiler Analytics Database. The DB connection now needs to be explicitly established with api$db_connect() and terminated with api$db_disconnect().
* Ensure that any open RPostgres DB connections are closed whenProfilerAPI R6 object is garbage collected.
* Added a new function `DataLake$search()` to search files and datasets across studies, using the Advanced Search. The result set contains columns for all metadata attributes across all result objects.
* Extended the `DataLake$list()` function to optionally include all metadata into the result set.
* Fixed the file upload of empty files in `DataLake$upload_file()`.
* Fixed a problem when downloading files using `DataLake$download_file()`. When the download fails, no corrupt file is created on disk.
* Renamed `WorkflowSessions$cancel()` to `WorkflowSessions$delete()`. `WorkflowSessions$cancel()` has become deprecated.  
* Improved handling of login response in the presence of connection problems to Profiler server.
* Deprecated methods have become defunct.

# ProfilerAPI2 2.3.1 (2021-10-15)
* Fixed a problem when downloading files via `DataLake$download_file()`. When the download fails, no corrupt file is created on disk.

# ProfilerAPI2 2.3.0 (2021-10-06)
* Fixed a problem related to erroneously skipped rows in Analytics View result sets that occurred in case all values are empty.
* ProfilerAPI2 now uses the new `moduleServer()` style of invoking server modules. Consequently, the shiny dependency has been updated to shiny version >= 1.5.0 (previously >= 1.2.0). This deprecates the usage of the `shiny::callModule()` function to call a shiny module in the server part. Rather call the shiny module by directly using the respective module server function `profiler_auth_mod_server("id")` or `profiler_sso_mod_server("id")`. This change will now also allow for testing Shiny apps the shinytest package.
* Authentication tokens generated via `profiler_sso_mod_server()` and `profiler_auth_mod_server()` in Shiny Apps are now stored as browser cookies to reduce re-authentication calls. This reduces potential flickering in Shiny apps using the SSO.
* In case the environment variable PROFILER_SECRET_API_KEY is set, shiny modules will now skip the oauth authentication flow and will automatically authenticate using the provided api key.
* The recursive listing of Data Lake folders via `DataLake$list()` is now much faster due to the fact that the recursion is now performed within the Profiler Core application. The resulting tibble will no longer contain the relative path of the returned objects but instead the parent folder ID is returned. Consequently, the `match_path` option is no longer available. Please note that this change requires Profiler >= 15.0.12.
* Flattened the complex status element in `WorkflowSessions$list()` and `WorkflowSessions$details()` to now report separate entries for message, error and state and reordered resulting columns.
* Logout button in `profiler_auth_mod_ui()` no longer erroneously resets the entire Shiny session but merely logs user out via ProfilerAPI$clear()
* Ensure the "saving credentials" message is shown immediately after refreshing a token. 
* Fixed wrong code reference in deprecation warning for `api$fetchWorkflowConfiguration()`.
* Use profile name `default` instead of `myprofile` in docs and vignettes.

# ProfilerAPI2 2.2.0 (2021-09-06)
* Added a new function `DataLake$download_folder()` to download a folder including all it's contents from the data lake.
* Added `Studies$study_id()` and `AnalyticsViews$view_id()` functions to retrieve the study/view id by the study/view name.
* It is now possible to use the workflow name instead of the workflow ID when calling `Workflows$details()` and `Workflows$config()`. 
* Allow to directly authenticate via `profiler_api()` when the PROFILER_SECRET_API_KEY environment variable is set.
* Added a function `ProfilerAPI$print_env_vars()` to print the values that were used to create a `ProfilerAPI` object. When settings the respective environment variables (e.g. PROFILER_SECRET_API_KEY), previously obtained credentials can be reused in an automated authentication flow.
* The `Studies$list()`, `DataLake$list()`, and the `AnalyticsViews$list()` functions now allow to filter the results by a pattern.
* All `list()` functions now return (empty) named tibble objects in case nothing could be listed.
* The `DataLake$create_folder()` function now functions as a `ensure` method, it returns the folder id with the specified name if the folder already exists, otherwise a new folder is created. Instead of a name, also a relative path is accepted, to specify the folder hierarchy to be ensured.
* The `DataLake$object_id()` function returns NULL if an object does not exist and does not fail anymore. An optional `study_group` parameter was added to the `DataLake$object_id()` function allowing to unambiguously specify studies by name.
* The `DataLake$upload_folder()` function now returns the folder id of the newly created folder.
* It is now possible to configure the names of Analytics Views and Analytics Tables via the workflow configuration.
* Improved usability by allowing to use a workflow name or workflow path instead of an actual workflow id when fetching workflow details or workflow config.
* Remove trailing slash from provided custom config path in `profiler_api()`.
* Fixed the error message in the `DataLake$object_id()` function in case the object could not be resolved unambiguously.
* Fixed a problem in the DBI connector that could lead to errors when opening a database table from the RStudio connection viewer pane.
* Improved error message when trying to load from a profile that does not exist. The error message now explains to the user to first authenticate and then explicitly to save the profile.
* Improved error message when REST API returns a 403 error code during authentication.
* Consolidated vignettes and added a new cheatsheet for quick reference.
* Test improvements.
* Updated the renv.lock file.

# ProfilerAPI2 2.1.2 (2021-06-02)
* Allow configuring externalFileSelectionFields in a `WorkflowConfiguration` (only usable with Profiler >= 15.0.8).
* During workflow configuration, check if the field allows multiple values or even an empty selection.
* Added a new function `WorkflowConfiguration$is_runnable()` to check whether all mandatory fields are configured.
* Added a new function `DataLake$upload_folder()` to upload a folder including all of its contents to a parent folder in the data lake.
* Fixed `DataLake$upload_multiple_files()` if no source ids were provided.
* Ensure that any trailing slashes in the server URL argument of the `profiler_config` object are always trimmed away.
* Changed default value for `show_progress` in `DataLake$download_file_raw()` to FALSE.
* Updated shipped test workflow to use the External Table Creation activity instead of the Internal Table Creation activity.

# ProfilerAPI2 2.1.1 (2021-05-12)
* Added `ProfilerAPI$data_lake$object_id()` function to retrieve the object id of any object in a study based on its path.
* Added `ProfilerAPI$data_lake$download_file_raw()` function to download raw file contents into memory.
* Fixed a problem with recursive object listing with a metadata filter.  
* Added functionality to upload workflow xml files via `ProfilerAPI$workflows$upload()` (only usable with Profiler >= 15.0.7).
* Provide a `module` parameter to `ProfilerAPI$workflows$list()` and `ProfilerAPI$workflow_sessions$list()` functions to filter listing of workflows/workflow sessions to the respective modules (only usable with Profiler >= 15.0.7).
* Fixed authentication error regarding refresh token that could occur when loading the API from a saved profile.
* Fixed deprecated `ProfilerAPI$fetchWorflowConfiguration` function.

# ProfilerAPI2 2.1.0 (2021-03-09)
* Split API into sub-modules.
* Allow for saving and loading API configuration and credentials via `ProfilerAPI$save_profile()` and the constructor function `profiler_api(profile)`.
* Adapted Profiler R API to follow the official tidyverse guidelines for code style and method naming. Some API methods have been renamed and have been deprecated. The old methods are still usable until they will be removed in a future release of the R-API.
* Use R6 class for workflow configuration instead of lists. The `ProfilerAPI$workflows$config()` function does now return R6 `WorkflowConfiguration` objects, which can be passed to `ProfilerAPI$workflows$run()` and `ProfilerAPI$workflow_sessions$run()`. The `ProfilerAPI$runWorkflow()` function is deprecated and does still take lists as returned by `fetch_workflow_configuration()` (deprecated). 
* Added functions `ProfilerAPI$workflow_sessions$wait_for()` and `ProfilerAPI$waitForWorkflowToFinish()` to monitor workflow execution status.
* Added option for recursive object listing to `ProfilerAPI$data_lake$list()` and `ProfilerAPI$listObjects()`.
* Updated ProfilerAPI2 vignette to document usage of Global Analytics Tables.
* Use POSIXct data type for datetime values in details and list methods for AnalyticsViews, DataLake, GlobalAnalyticsTables, Studies, Workflows, and WorkflowSessions.  
* Improved documentation of R6 class `ProfilerAPI`.  
* Replaced implementation of `DBI::dbExistsTable` by a more efficient one.
* Improved tests.

# ProfilerAPI2 2.0.1 (2020-10-29)
* Added new module for Shiny Apps (`modProfilerSSOAuthUI`, `modProfilerSSOAuthServer`) that can be used to authenticate without clicking a login button. 
* Added dependency on shinyjs because of the new module.
* Removed outdated dependency on reactR.
* Removed support for recursive object listing.
* Fixed test, so it will not fail with updated Profiler versions.
* Fixed a problem with connection observer when using DBI connection from IntelliJ IDE.

# ProfilerAPI2 2.0.0 (2020-09-04)
* The ProfilerAPI2 package supports now automated data upload and processing via REST
	* Folder creation in the Data Lake
	* File up- and downloading to/from the Data Lake
	* Workflow configuration and execution
* The ProfilerAPI2 package supports creation, modification, and deletion of global analytics tables
* The profiler_connector_test_package (in extdata) has been extended and provides a workflow to run the new Automation vignette and additional tests. See README.md for installation instructions. 

# General
Note: This document summarizes changes in the ProfilerAPI2 package by package version, which does not correspond to Genedata Profiler Software versions!
