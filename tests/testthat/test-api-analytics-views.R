test_that("view listing", {
    api <- get_api()
    study_id <- get_test_study_id()
    view_id <- get_expression_test_data_view_id()

    expect_output(api$analytics_views$print(), "<AnalyticsViews>")

    all_views <- api$analytics_views$list()
    study_views <- api$analytics_views$list(study_id)
    expect_true(inherits(all_views, "tbl"))

    expect_true(all(get_exp_view_listing_colnames() %in% colnames(all_views)))
    expect_true(nrow(all_views) >= nrow(study_views))
    expect_true(view_id %in% all_views$id)
    expect_true(view_id %in% study_views$id)

    filtered_views <- api$analytics_views$list(study_id, metadata = list("R-Test"="true"))
    expect_true(view_id %in% filtered_views$id)
})

test_that("Filter view listing by pattern", {
    api <- get_api()
    study_id <- get_test_study_id()
    view_id <- get_expression_test_data_view_id()
    filtered_views <- api$analytics_views$list(study_id, pattern = stringr::fixed("EXPRESSION_", ignore_case = TRUE))
    expect_true(view_id %in% filtered_views$id)
    filtered_views <- api$analytics_views$list(study_id, pattern = stringr::fixed("EXPRESSION_", ignore_case = FALSE))
    expect_false(view_id %in% filtered_views$id)
})

test_that("Allow filter for executable views", {
    api <- get_api()

    # ensure executable views are a subset of all views
    all_views <- api$analytics_views$list(executable_only = FALSE)
    executable_views <- api$analytics_views$list(executable_only = TRUE)
    expect_true(nrow(executable_views) <= nrow(all_views))

    # ensure that per default executable_only is set to TRUE
    expect_equal(nrow(api$analytics_views$list()), nrow(executable_views))

    # ensure that executable_only only excepts TRUE or FALSE
    expect_error(api$analytics_views$list(executable_only = 1))
    expect_error(api$analytics_views$list(executable_only = "FALSE"))
    expect_error(api$analytics_views$list(executable_only = c(TRUE, FALSE)))
})

test_that("view details", {
    api <- get_api()
    view_id <- get_expression_test_data_view_id()
    details <- api$analytics_views$details(view_id)

    expect_true(inherits(details, "list"))
    expect_true(all(get_exp_view_details_names() %in% names(details))) # do not enforce setequal as we cannot be 100%sure that "latestViewAlias" is present
    expect_equal(details$id, view_id)
    expect_equal(details$columnCount, 13)
    expect_equal(details$rowCount, 32)

    col_names <- c("rowid", "usubjid", "pfgenri", "pfrepnum", "pfgenloc", "pfgensr", "pforres", "pforresu", "pfstresc", "pfstresu", "date_field", "pfdtc", "boolean_field")
    col_types <- c("INTEGER", "INTEGER", "VARCHAR", "INTEGER", "VARCHAR", "INTEGER", "DOUBLE PRECISION", "VARCHAR", "DOUBLE PRECISION", "VARCHAR", "DATE", "TIMESTAMP", "BOOLEAN")
    exp_schema <- data.frame("name" = col_names,  "type" = col_types, stringsAsFactors = FALSE)
    expect_equal(details$schema, exp_schema)
})


test_that("view fetching", {
    api <- get_api()
    view_id <- get_expression_test_data_view_id()
    view <- api$analytics_views$fetch(view_id) %>% arrange(rowid)
    expect_true(inherits(view, "tbl"))
    expect_equal(nrow(view), 32)
    expect_equal(ncol(view), 13)

    expected_df <- get_expression_test_data_expected_df()
    testthat::expect_true(all_equal(view, expected_df, ignore_col_order = FALSE, ignore_row_order = FALSE))
})

test_that("Added `view_id()` function to retrieve the id by the name.", {
    api <- get_api()
    study_id <- get_test_study_id()
    view_id <- get_expression_test_data_view_id()
    view_name <- api$analytics_views$details(view_id)$name
    expect_equal(api$analytics_views$view_id(view_name), view_id)
    expect_equal(api$analytics_views$view_id(view_name, study_id = study_id), view_id)
    view_name_lower <- toupper(view_name)
    expect_warning(v <- api$analytics_views$view_id(view_name_lower, ignore_case = FALSE), paste0("No view found with name '", view_name_lower, "'."))
    expect_null(v)
})

test_that("Ensure empty rows are not skipped", {
    api <- get_api()
    view_id <- get_expression_test_data_view_id()
    subject_id_col <- api$analytics_views$fetch(view_id) %>% filter(pfgenloc == "chr6") %>% pull("usubjid")
    expect_equal(c(1101, 1101, 1102, NA), subject_id_col)
})

test_that("View versioning", {
    api <- get_api()
    study_id <- get_test_study_id()

    new_folder_name <- paste("R API Test View Versioning", format(Sys.time(), "%Y-%m-%d %H-%M-%S"))
    folder_id <- api$data_lake$create_folder(ensure_test_results_folder(), name = new_folder_name)
    view_name <- paste(study_id, "r_api_versioning_test", sep="_")
    table_file <- system.file("extdata", 'expression_test_data.tsv', package = "ProfilerAPI2")
    table_id <- api$analytics_tables$create_table_from_file(table_file, folder_id, name = view_name, metadata = list('R-Test'='true'))
    view_id_original <- api$analytics_views$create_view_from_table(table_id, folder_id)

    details_original <- api$analytics_views$details(view_id_original)
    expect_true(details_original$latest)
    match <- stringr::str_match(details_original$id, "(\\w+)_(\\d+)$")
    expect_equal(details_original$baseId, match[1, 2])
    expect_equal(details_original$version, as.integer(match[1, 3]))

    # rerun workflow to have a newer version
    view_id_latest <- api$analytics_views$create_view_from_table(table_id, folder_id)
    details_latest <- api$analytics_views$details(view_id_latest)
    details_original_updated <- api$analytics_views$details(view_id_original)

    expect_true(details_latest$latest)
    expect_equal(details_latest$baseId, details_original$baseId)
    expect_equal(details_latest$latestViewAlias, paste0(details_latest$baseId, "_latest"))
    expect_equal(details_latest$id, paste(details_latest$baseId, details_latest$version, sep = "_"))

    expect_false(details_original_updated$latest)
    api$data_lake$delete(folder_id)
})

test_that("Save SQL query as view", {
    api <- get_api()
    view_id <- get_expression_test_data_view_id()
    folder_id <- ensure_folder_exists(ensure_test_results_folder(), "R API View Creation", api)
    view_name <- paste("R API Filtered View", format(Sys.time(), "%Y-%m-%d %H-%M-%S"))
    target_gene <- "FOXM1|HGNC:3818|ENSG00000111206"

    query <- api$conn %>% tbl(view_id) %>%
      dplyr::filter(pfgenri == target_gene) %>%
      dbplyr::sql_render() %>%
      stringr::str_replace("<SQL> ", "")
    new_view_id <- api$analytics_views$create_view_from_sql(query, folder_id, view_name)

    expect_false(is.null(new_view_id))
    expect_true(all(pull(tbl(api$conn, new_view_id), pfgenri) == target_gene))
})

test_that("Save SQL query referencing view by object id", {
    api <- get_api()
    view_id <- get_expression_test_data_view_id()
    object_id <- api$analytics_views$details(view_id)$objectId
    folder_id <- ensure_folder_exists(ensure_test_results_folder(), "R API View Creation", api)
    view_name <- paste("R API Custom View", format(Sys.time(), "%Y-%m-%d %H-%M-%S"))

    query <- glue::glue('SELECT * FROM "{table_name}"', table_name = object_id)
    new_view_id <- api$analytics_views$create_view_from_sql(query, folder_id, view_name)

    expect_false(is.null(new_view_id))
})

test_that("View Parameter Testing", {
    # When removed. Look into removing ProfilerAnalyticsTest.parque too.
    api <- get_api()
    view_ids <- get_test_view_ids()
    view_id <- view_ids$view1
    view_id_2 <- view_ids$view2

    details <- api$analytics_views$details(view_id)
    expect_equal(details$parameters$name, "dummy")
    expect_equal(details$parameters$type, "Boolean")

    expect_error(api$analytics_views$fetch(view_id), "Please provide values for all view parameters: dummy", fixed = TRUE)
    params <- list("dummy" = "false", "some_random_param" = "some_random_value")
    expect_message(view <- api$analytics_views$fetch(view_id, view_parameters = params), "The following parameters you provided were ignored: some_random_param", fixed=TRUE)
    view <- view %>% arrange(rowid)

    expect_true(inherits(view, "tbl"))
    expect_equal(nrow(view), 32)
    expect_equal(ncol(view), 14)

    view_without_params <- api$analytics_views$fetch(view_id_2, view_parameters = list()) %>% arrange(rowid)
    suppressWarnings(p <- vroom::problems(view_without_params)) # https://profiler.genedata.com/confluence/display/PRODOC16/Column+Types
    expect_gt(nrow(p), 0) # Java min int is returned as NA
    expect_true(inherits(view_without_params, "tbl"))
    expect_equal(nrow(view_without_params), 34)
    expect_equal(ncol(view_without_params), 14)

})


test_that("View creation from table with overwrite modes", {
    api <- get_api()
    study_id <- get_test_study_id()
    folder_id <- ensure_test_results_folder()
    table_name <- basename(tempfile(pattern = "external-table-"))
    view_name <- basename(tempfile(pattern = "view-"))
    table_file <- system.file("extdata", 'expression_test_data.tsv', package = "ProfilerAPI2")
    table_id <- api$analytics_tables$create_table_from_file(table_file, folder_id, name = table_name)

    # just create a view from dataframe and check that its name mathces
    view_id <- api$analytics_views$create_view_from_table(table_id, folder_id, name = view_name)
    view_details <- api$analytics_views$details(view_id)
    expect_match(view_details$name, view_name, fixed = TRUE)

    # creating a view with the same name and default overwrite mode results in automatic renaming
    view_id2 <- api$analytics_views$create_view_from_table(table_id, folder_id, name = view_name, overwrite = NA)
    view_details2 <- api$analytics_views$details(view_id2)
    expect_match(view_details2$name, glue::glue('{view_name} [1]'), fixed = TRUE)
    expect_false(view_id == view_id2)

    # creating a view with the same name with overwrite mode true should replace the view
    view_id3 <- api$analytics_views$create_view_from_table(table_id, folder_id, name = view_name, overwrite = TRUE)
    view_details3 <- api$analytics_views$details(view_id3)
    expect_match(view_details3$name, view_name, fixed = TRUE)
    expect_error(api$data_lake$details(view_id))
    expect_false(view_id == view_id3)

    # creating a view with the same name and overwrite mode false should fail
    expect_error(api$analytics_views$create_view_from_table(table_id, folder_id, name = view_name, overwrite = FALSE))

})

test_that("View creation from sql with overwrite modes", {
    api <- get_api()
    study_id <- get_test_study_id()
    folder_id <- ensure_test_results_folder()
    table_name <- basename(tempfile(pattern = "external-table-"))
    view_name <- basename(tempfile(pattern = "view-sql"))
    table_file <- system.file("extdata", 'expression_test_data.tsv', package = "ProfilerAPI2")
    table_id <- api$analytics_tables$create_table_from_file(table_file, folder_id, name = table_name)

    # just create a view from dataframe and check that its name mathces
    view_id <- api$analytics_views$create_view_from_sql(glue::glue('SELECT * FROM "{table_id}"'), folder_id, name = view_name)
    view_details <- api$analytics_views$details(view_id)
    expect_match(view_details$name, view_name, fixed = TRUE)

    # creating a view with the same name and default overwrite mode results in automatic renaming
    view_id2 <- api$analytics_views$create_view_from_sql(glue::glue('SELECT * FROM "{table_id}"'), folder_id, name = view_name, overwrite = NA)
    view_details2 <- api$analytics_views$details(view_id2)
    expect_match(view_details2$name, glue::glue('{view_name} [1]'), fixed = TRUE)
    expect_false(view_id == view_id2)

    # creating a view with the same name with overwrite mode true should replace the view
    view_id3 <- api$analytics_views$create_view_from_sql(glue::glue('SELECT * FROM "{table_id}"'), folder_id, name = view_name, overwrite = TRUE)
    view_details3 <- api$analytics_views$details(view_id3)
    expect_match(view_details3$name, view_name, fixed = TRUE)
    expect_error(api$data_lake$details(view_id))
    expect_false(view_id == view_id3)

    # creating a view with the same name and overwrite mode false should fail
    expect_error(api$analytics_views$create_view_from_sql(glue::glue('SELECT * FROM "{table_id}"'), folder_id, name = view_name, overwrite = FALSE))

})

test_that("View listing with metadata", {
    api <- get_api()
    study_id <- get_test_study_id()
    view_id <- get_expression_test_data_view_id()

    study_views <- api$analytics_views$list(study_id)
    expect_true(all(get_exp_view_listing_colnames() %in% colnames(study_views)))

    study_views_md <- api$analytics_views$list(study_id, all_attributes=T)
    expect_true(all(get_exp_view_listing_colnames() %in% colnames(study_views_md)))
    expect_true(nrow(study_views) == nrow(study_views_md))
    expect_true(ncol(study_views) <= ncol(study_views_md))
    expect_true(all(study_views$id ==  study_views_md$id))
    expect_true("R-Test" %in% colnames(study_views_md))

    filtered_views <- api$analytics_views$list(study_id, metadata = list("R-Test"="true"), all_attributes=T)
    expect_true(view_id %in% filtered_views$id)
    expect_true(filtered_views %>% pull("R-Test") %>% first() == "true")
})
